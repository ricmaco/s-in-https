This is the home of resources for the talk I gave at the 2018 edition of the
Italian LinuxDay.

## Folders

`src` folder is for original sources of resources used in the presentation.
`res` is for the actual resources used in the presentatin.

`presentation.*` is the actual presentation in specified format.

## License

*La S in HTTPS* (c) by Riccardo Macoratti <<r.macoratti@gmx.co.uk>>

*La S in HTTPS* is licensed under a
Creative Commons Attribution-ShareAlike 3.0 Unported License.

You should have received a copy of the license along with this
work. If not, see [here](http://creativecommons.org/licenses/by-sa/3.0/).
